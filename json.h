#ifndef __JSON__H__
#define __JSON__H__

#include "stdio.h"
#include "ctype.h"

/*----------------------------------------------------------------------------
---- Buffers 
--------------------------------------------------------------------------*/

enum JBufferType
{
	File
};

struct JBuffer
{
	void* ptr;
	int size;
	int position;

	char (*getch)(struct JBuffer*); 
	void (*move)(struct JBuffer*, int);

	void (*close)(struct JBuffer*);
};

void JCloseBuffer(struct JBuffer*);

void JCreateFileBuffer(struct JBuffer*, char filename[]);


/*----------------------------------------------------------------------------
---- Parser
--------------------------------------------------------------------------*/


#ifndef RATIONAL_TYPE
#define RATIONAL_TYPE double
#endif

#ifndef INTEGER_TYPE
#define INTEGER_TYPE int
#endif

enum JParserState
{
	WaitTableClose,
	WaitArrayClose,
	WaitName,	
	WaitDivider,
	WaitValue
};

struct JParser
{
	struct JBuffer* buffer;

	void (*onTable)(char name[]);
	void (*onTableClose)();
	void (*onArray)(char name[]);
	void (*onArrayClose)();
	void (*onString)(char name[], char value[]);	
	void (*onInteger)(char name[], INTEGER_TYPE value);
	void (*onDouble)(char name[], RATIONAL_TYPE value);	
	void (*onBoolean)(char name[], char value);

	enum JParserState state;
};

void JParserInit(struct JParser*, struct JBuffer*,
				 void (*onTable)(char name[]),
				 void (*onTableClose)(),
				 void (*onArray)(char name[]),
				 void (*onArrayClose)(),
				 void (*onString)(char name[], char value[]),
				 void (*onInteger)(char name[], INTEGER_TYPE value),
				 void (*onDouble)(char name[], RATIONAL_TYPE value),
				 void (*onBoolean)(char name[], char value));

void JParse(struct JParser*);

#endif