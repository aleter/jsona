#include "json.h"
#include "assert.h"
#include "stdlib.h"


#define JSTRCPY(to, from) { int ii__ = 0; while((to[ii__++]=from[ii__])!='\0'){}; }

/*----------------------------------------------------------------------------
---- Buffers 
--------------------------------------------------------------------------*/

void JCloseBuffer(struct JBuffer* buffer)
{
	buffer->close(buffer);
}


 static char j_file_buffer_getch(struct JBuffer* data)
 {
	 FILE* f = (FILE*)(data->ptr);
	 char result;
	 size_t count = fread(&result, sizeof(char), 1, f);	 
	 assert(data->position<data->size);
	 data->position++;
	 return result;
 }

 static void j_file_buffer_seek(struct JBuffer* data, int count)
 {
	 FILE* f = (FILE*)(data->ptr);
	 fseek(f , SEEK_CUR, count);
	 data->position+=count;
	 assert(data->position>=0);
	 assert(data->position<data->size);
 }

 static void j_file_buffer_close(struct JBuffer* data)
 {
	 FILE* f = (FILE*)(data->ptr);
	 fclose(f);
 }

 void JCreateFileBuffer(struct JBuffer* buffer, char filename[])
 {
	 FILE* f;
	 errno_t err = fopen_s( &f, filename, "r");
	 assert(err==0);
	 buffer->ptr = (void*)f;

	 fseek(f, 0L, SEEK_END);
	 buffer->size = ftell(f);
	 fseek(f, 0L, SEEK_SET);

	 buffer->position = 0;

	 buffer->getch = j_file_buffer_getch;
	 buffer->move = j_file_buffer_seek;
	 buffer->close = j_file_buffer_close;
 }



 /*----------------------------------------------------------------------------
---- Parser
--------------------------------------------------------------------------*/

void JParserInit(struct JParser* parser, struct JBuffer* buffer,
				 void (*onTable)(char name[]),
				 void (*onTableClose)(),
				 void (*onArray)(char name[]),
				 void (*onArrayClose)(),
				 void (*onString)(char name[], char value[]),
				 void (*onInteger)(char name[], INTEGER_TYPE value),
				 void (*onDouble)(char name[], RATIONAL_TYPE value),
				 void (*onBoolean)(char name[], char value))
 {
	 parser->buffer = buffer;
	 parser->onArray = onArray;
	 parser->onBoolean = onBoolean;
	 parser->onDouble = onDouble;
	 parser->onInteger = onInteger;
	 parser->onString = onString;
	 parser->onTable = onTable;
	 parser->onTableClose = onTableClose;
	 parser->onArrayClose = onArrayClose;
 }

static const char j_parser_table_open_char = '{';
static const char j_parser_table_close_char = '}';

static const char j_parser_array_open_char = '[';
static const char j_parser_array_close_char = ']';

static const char j_parser_string_open_char = '"';
static const char j_parser_divider_char = ':';

static const char j_parser_boolean_true[] = "\x04 true";
static const char j_parser_boolean_false[] = "\x05 false";

enum j_type_stack {
	Table,
	Array
};
#define J_TYPE_TEMP_STACK_SIZE_ 255
static enum j_type_stack j_type_stack_temp_[J_TYPE_TEMP_STACK_SIZE_];
static int j_type_stack_temp_pointer_ = 0;

static enum j_type_stack j_type_stack_top()
{
	assert(j_type_stack_temp_pointer_>=0);
	return j_type_stack_temp_[j_type_stack_temp_pointer_-1];
}

static enum j_type_stack j_type_stack_pop()
{
	assert(j_type_stack_temp_pointer_>0);
	return j_type_stack_temp_[--j_type_stack_temp_pointer_];
}

static void j_type_stack_push(enum j_type_stack type)
{
	assert(j_type_stack_temp_pointer_<(J_TYPE_TEMP_STACK_SIZE_-1));
	j_type_stack_temp_[j_type_stack_temp_pointer_++] = type;
}

char jp_key_buf_[255];
char jp_value_buf_[255];

 void JParse(struct JParser* parser)
 {
	 char cchar;
	 char* keyBuf = jp_key_buf_;
	 char* valBuf = jp_value_buf_;
	 int isRecentlyOpened = 0;
	 assert(parser->buffer);	 
	 parser->state = WaitValue;
	 JSTRCPY(keyBuf, "root\0");

	 cchar = parser->buffer->getch(parser->buffer);
	 while(parser->buffer->position < parser->buffer->size)
	 {		 
		 switch(parser->state)
		 {

		case WaitTableClose:
			{
				while(isspace(cchar)){cchar = parser->buffer->getch(parser->buffer);};
				if(cchar==j_parser_table_close_char)
				{
					j_type_stack_pop();
					parser->state = ((j_type_stack_top()==Table)? WaitTableClose : WaitArrayClose );	
					parser->onTableClose();
					cchar = parser->buffer->getch(parser->buffer);
				}else if(cchar==',')
				{					
					parser->state = WaitName;
					cchar = parser->buffer->getch(parser->buffer);
					//continue;
				}else if(isRecentlyOpened == 1)
				{			
					isRecentlyOpened = 0;
					parser->state = WaitName;
					//continue;
				}else 
				{
					assert(0);
				}
			} continue;

		case WaitArrayClose:
			{
				while(isspace(cchar)){cchar = parser->buffer->getch(parser->buffer);};
				if(cchar==j_parser_array_close_char)
				{
					j_type_stack_pop();
					parser->state = ((j_type_stack_top()==Table)? WaitTableClose : WaitArrayClose );		
					parser->onArrayClose();
					cchar = parser->buffer->getch(parser->buffer);
				}else if(cchar==',')
				{					
					parser->state = WaitValue;
					cchar = parser->buffer->getch(parser->buffer);
					//continue;
				}else if(isRecentlyOpened == 1)
				{			
					isRecentlyOpened = 0;
					parser->state = WaitValue;
				}else 
				{
					assert(0);
				}
			} continue;

		case WaitName:
			{
				while(isspace(cchar)){cchar = parser->buffer->getch(parser->buffer);};
				if(cchar==j_parser_string_open_char)
				{
					int i = 0;
					while((cchar = parser->buffer->getch(parser->buffer))!='"')
					{
						keyBuf[i++] = cchar;
					}
					keyBuf[i] = '\0';
					cchar = parser->buffer->getch(parser->buffer);
					parser->state = WaitDivider;
				}else 
				{
					assert(0);
				}
			} continue;

		case WaitDivider:
			{
				while(isspace(cchar)){cchar = parser->buffer->getch(parser->buffer);};
				if(cchar==j_parser_divider_char)
				{
					parser->state = WaitValue;
					cchar = parser->buffer->getch(parser->buffer);
				}else 
				{
					assert(0);
				}
			} continue;

		case WaitValue:
			{				
				while(isspace(cchar)){cchar = parser->buffer->getch(parser->buffer);};
				if(cchar==j_parser_string_open_char)
				{
					/* value - string */
					int i = 0;
					while((cchar = parser->buffer->getch(parser->buffer))!='"')
					{
						valBuf[i++] = cchar;
					}
					valBuf[i] = '\0';
					parser->onString(keyBuf, valBuf);
					parser->state = ((j_type_stack_top()==Table)? WaitTableClose : WaitArrayClose );
					cchar = parser->buffer->getch(parser->buffer);
				}else if(cchar==j_parser_table_open_char)
				{
					/* value - table */
					j_type_stack_push(Table);
					cchar = parser->buffer->getch(parser->buffer);
					isRecentlyOpened = 1;
					parser->onTable(keyBuf);
					parser->state = WaitTableClose;
				}else if(cchar==j_parser_array_open_char)
				{
					/* value - array */
					j_type_stack_push(Array);
					cchar = parser->buffer->getch(parser->buffer);
					isRecentlyOpened = 1;
					parser->onArray(keyBuf);
					parser->state = WaitArrayClose;
				}else if(isdigit(cchar) || cchar=='-')
				{
					/* value - table */
					int i = 0;
					int isReal = 0;
					valBuf[i++] = cchar;
					while( isdigit( cchar = parser->buffer->getch(parser->buffer) ) ) 
					{
						valBuf[i++] = cchar;
					};
					if(cchar=='.')
					{
						isReal = 1;
						valBuf[i++] = cchar;
						while( isdigit( cchar = parser->buffer->getch(parser->buffer) ) ) 
						{
							valBuf[i++] = cchar;
						};
					}
					valBuf[i] = '\0';
					if(isReal!=0)
					{
						double val = atof(valBuf);
						parser->onDouble(keyBuf, val);
					}else{
						int val = atoi(valBuf);
						parser->onInteger(keyBuf, val);
					}
					parser->state = ((j_type_stack_top()==Table)? WaitTableClose : WaitArrayClose );
				}else if( cchar==j_parser_boolean_true[2])
				{
					int i;
					for(i=1;i<j_parser_boolean_true[0];i++)
					{
						cchar = parser->buffer->getch(parser->buffer);
						if(cchar != j_parser_boolean_true[i+2])
						{
							assert(0);							
						}
					}
					cchar = parser->buffer->getch(parser->buffer);
					parser->onBoolean(keyBuf, 1);
					parser->state = ((j_type_stack_top()==Table)? WaitTableClose : WaitArrayClose );
				}else if(cchar==j_parser_boolean_false[2])
				{
					int i;
					for(i=1;i<j_parser_boolean_false[0];i++)
					{
						cchar = parser->buffer->getch(parser->buffer);
						if(cchar != j_parser_boolean_false[i+2])
						{
							assert(0);							
						}
					}
					cchar = parser->buffer->getch(parser->buffer);
					parser->onBoolean(keyBuf, 0);
					parser->state = ((j_type_stack_top()==Table)? WaitTableClose : WaitArrayClose );
				}else
				{
					assert(0);
				}
			} continue;
		 };


	 }
 }
