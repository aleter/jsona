#include "stdio.h"

#include "json.h"

static void onTable(char name[])
{
	printf("\nTable %s", name);

}

static void onTableClose()
{	
	printf("\nTable closed");
}

static void onArray(char name[])
{
	printf("\nArray %s", name);
}

static void onArrayClose()
{
	printf("\nArray closed");
}

static void onString(char name[], char value[])
{
	printf("\n %s - %s", name, value);
}

static void onInteger(char name[], INTEGER_TYPE value)
{	
	printf("\n %s - %d", name, value);
}

static void onDouble(char name[], RATIONAL_TYPE value)
{
	printf("\n %s - %f", name, value);
}

static void onBoolean(char name[], char value)
{
	//static const char true_str[] = "true";
	//static const char false_str[] = "false";
	printf("\n %s - %s", name, ((value!=0)?"true" :"false") );
}



int main(int argc, char** args)
{
	struct JBuffer jbuf;
	struct JParser jparser;
	JCreateFileBuffer(&jbuf, "test.json");
	JParserInit(&jparser, &jbuf, onTable, onTableClose, onArray, onArrayClose, onString, onInteger, onDouble, onBoolean); 
	JParse(&jparser);
	JCloseBuffer(&jbuf);
	getchar();
	return 0;
}



